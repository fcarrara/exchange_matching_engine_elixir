defmodule MatchingEngineTest do
  use ExUnit.Case
  doctest MatchingEngine

  test "creates a map from the input.json file" do
    assert MatchingEngine.read_and_decode_json("input1.json") ==
      %Input{
        orders: [
          %Order{amount: 2.4, command: "sell", price: 100.003},
          %Order{amount: 3.445, command: "buy", price: 90.394}
        ]
      }
  end

  test "insert Buy and Sell orders when the lists are empty" do
    input = %Input{
              orders: [
                %Order{amount: 2.4, command: "sell", price: 100.003},
                %Order{amount: 3.445, command: "buy", price: 90.394}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy: [%Record{price: 90.394, volume: 3.445}], 
        sell: [%Record{price: 100.003, volume: 2.4}]
      }
  end

  test "records in buy list should be ordered by price descendant" do
    input = %Input{
              orders: [
                %Order{amount: 1.4, command: "buy", price: 80.03},
                %Order{amount: 2.3, command: "buy", price: 70.003},
                %Order{amount: 2.4, command: "buy", price: 100.003},
                %Order{amount: 3.45, command: "buy", price: 90.394}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy: [%Record{price: 100.003, volume: 2.4},
              %Record{price: 90.394, volume: 3.45},
              %Record{price: 80.03, volume: 1.4},
              %Record{price: 70.003, volume: 2.3}],
        sell: []
      }
  end

  test "records in sell list should be ordered by price assendant" do
    input = %Input{
              orders: [
                %Order{amount: 1.4, command: "sell", price: 80.03},
                %Order{amount: 2.3, command: "sell", price: 70.003},
                %Order{amount: 2.4, command: "sell", price: 100.003},
                %Order{amount: 3.45, command: "sell", price: 90.394}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy: [],
        sell: [%Record{price: 70.003, volume: 2.3},
              %Record{price: 80.03, volume: 1.4},
              %Record{price: 90.394, volume: 3.45},
              %Record{price: 100.003, volume: 2.4}]
      }
  end

  test "sum volume when two orders have same price and command" do
    input = %Input{
              orders: [
                %Order{amount: 2.4, command: "buy", price: 80.05},
                %Order{amount: 3.445, command: "sell", price: 100.003},
                %Order{amount: 3.2, command: "buy", price: 80.05},
                %Order{amount: 1.4, command: "sell", price: 100.003}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy: [%Record{price: 80.05, volume: 5.6}], 
        sell: [%Record{price: 100.003, volume: 4.845}]
      }
  end

  test "sell record is removed when buy order is compared and there is not amount left" do
    input = %Input{
              orders: [
                %Order{amount: 2.4, command: "sell", price: 80.05},
                %Order{amount: 2.4, command: "buy", price: 80.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy: [%Record{price: 80.05, volume: 2.4}], 
        sell: []
      }
  end

  test "first sell record is removed and second one updated when buy order is 
        compared and there is amount left" do
    input = %Input{
              orders: [
                %Order{amount: 1.0, command: "sell", price: 80.05},
                %Order{amount: 2.0, command: "sell", price: 70.03},
                %Order{amount: 2.2, command: "buy", price: 80.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy: [], 
        sell: [%Record{price: 80.05, volume: 0.8}]
      }
  end

  test "sell record is removed and buy record is added with the amount 
        left when buy order is same as sell and amount left is negative" do
    input = %Input{
              orders: [
                %Order{amount: 1.0, command: "sell", price: 80.05},
                %Order{amount: 2.0, command: "sell", price: 70.03},
                %Order{amount: 5.2, command: "buy", price: 80.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy: [%Record{price: 80.05, volume: 2.2}], 
        sell: []
      }
  end

  test "add new record into buy side when buy is less price than sell in output" do
    input = %Input{
              orders: [
                %Order{amount: 1.0, command: "sell", price: 80.05},
                %Order{amount: 2.0, command: "sell", price: 70.03},
                %Order{amount: 5.2, command: "buy", price: 40.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy:  [%Record{price: 40.05, volume: 5.2}], 
        sell: [%Record{price: 70.03, volume: 2.0},
               %Record{price: 80.05, volume: 1.0}]
      }
  end

  test "remove sell record and update buy record when buy price is bigger than sell price" do
    input = %Input{
              orders: [
                %Order{amount: 1.0, command: "sell", price: 80.05},
                %Order{amount: 5.2, command: "buy", price: 90.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy:  [%Record{price: 90.05, volume: 4.2}], 
        sell: []
      }
  end

  test "update buy and sell list when buy price is bigger than sell price and there is amount left" do
    input = %Input{
              orders: [
                %Order{amount: 4.0, command: "sell", price: 80.05},
                %Order{amount: 1.2, command: "buy", price: 90.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy:  [%Record{price: 90.05, volume: 1.2}], 
        sell: [%Record{price: 80.05, volume: 2.8}]
      }
  end

  test "sell record is removed and buy record is added with the amount 
        left when buy order is bigger than sell and amount left is negative" do
    input = %Input{
              orders: [
                %Order{amount: 1.0, command: "sell", price: 80.05},
                %Order{amount: 2.0, command: "sell", price: 70.05},
                %Order{amount: 5.2, command: "buy", price: 90.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy:  [%Record{price: 90.05, volume: 2.2}], 
        sell: []
      }
  end

  test "buy record is removed when sell order is compared and there is not amount left" do
    input = %Input{
              orders: [
                %Order{amount: 2.2, command: "buy", price: 80.05},
                %Order{amount: 2.2, command: "sell", price: 80.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy:  [], 
        sell: [%Record{price: 80.05, volume: 2.2}]
      }
  end

  test "first buy record is removed and sell is updated when sell order is 
        compared and there is amount left" do
    input = %Input{
              orders: [
                %Order{amount: 1.0, command: "buy", price: 90.05},
                %Order{amount: 2.0, command: "buy", price: 80.05},
                %Order{amount: 2.2, command: "sell", price: 90.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy:  [%Record{price: 80.05, volume: 2.0}], 
        sell: [%Record{price: 90.05, volume: 1.2}]
      }
  end

  test "buy record is removed and sell record is added with the amount 
        left when sell order is same as buy and amount left is negative" do
    input = %Input{
              orders: [
                %Order{amount: 1.0, command: "buy", price: 90.05},
                %Order{amount: 2.0, command: "buy", price: 99.15},
                %Order{amount: 5.2, command: "sell", price: 90.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy:  [], 
        sell: [%Record{price: 90.05, volume: 2.2}]
      }
  end

  test "add new record into sell side when sell is bigger price than buy in output" do
    input = %Input{
              orders: [
                %Order{amount: 1.0, command: "buy", price: 99.05},
                %Order{amount: 2.0, command: "buy", price: 90.15},
                %Order{amount: 5.2, command: "sell", price: 132.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy:  [%Record{price: 99.05, volume: 1.0},
               %Record{price: 90.15, volume: 2.0}], 
        sell: [%Record{price: 132.05, volume: 5.2}]
      }
  end

  test "remove buy record and update sell record when sell price is less than buy price" do
    input = %Input{
              orders: [
                %Order{amount: 1.0, command: "buy", price: 99.05},
                %Order{amount: 5.2, command: "sell", price: 32.05}
              ]
            }

    assert MatchingEngine.generate_output(input.orders) ==
      %Output{
        buy:  [], 
        sell: [%Record{price: 32.05, volume: 4.2}]
      }
  end
end
