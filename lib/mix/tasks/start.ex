defmodule Mix.Tasks.Start do
  use Mix.Task

  def run(_), do: IO.inspect(MatchingEngine.run())
end
