require IEx

defmodule MatchingEngine do
  require Poison
  import Float
  import Input
  import Output

  @moduledoc """
  Omise Challenge - Matching Engine
  """


  @doc ~S"""
  Run the Matching Engine application.
  Returns an output.json file.
  """
  def run do
    read_and_decode_json("input4.json").orders
      |> generate_output
      |> encode_output_json
  end


  @doc ~S"""
  Read the input.json file and decode it using Poison.
  Returns the map to be used as input. 
  """
  def read_and_decode_json(filename) do
    {:ok, string} = File.read(filename)
    Poison.decode!(string, as: %Input{})
  end


  @doc ~S"""
  Encode the map generated as output and write it into a file.
  Returns the output.json file. 
  """
  def encode_output_json(output) do
    File.write!("output.json", Poison.encode!(output, pretty: true), [:binary])
  end


  @doc ~S"""
  Encode the map generated as output and write it into a file.
  Returns the output.json file. 
  """
  def generate_output(input) do
    Enum.reduce(input, %Output{}, fn order, output ->
      process_order(order, output, String.to_atom(order.command))
    end)
  end


  @doc ~S"""
  Process a buy order command.
  Returns the updated output map.
  """
  defp process_order(order, output, command) when command == :buy do
    if output.sell == [] do
      add_to_output(output, :buy, order)
    else
      {new_list, left} = reduce_sell(output.sell, order)
    
      # Update sell list with new list
      output = Map.put(output, :sell, new_list)

      # If order was not added in sell then we add it to buy
      if Enum.find(output.sell, &(&1.price == order.price)) == nil do
        order = if left > 0, do: %Order{order | amount: left}, else: order
        add_to_output(output, :buy, order)
      else
        output
      end
    end
  end


  @doc ~S"""
  Process a sell order command.
  Returns the updated output map.
  """
  defp process_order(order, output, command) when command == :sell do
    if output.buy == [] do
      add_to_output(output, :sell, order)
    else
      {new_list, left} = reduce_buy(output.buy, order)
        
      # Update buy list with new list
      output = Map.put(output, :buy, new_list)

      # If order was not added in buy then we add it to sell
      if Enum.find(output.buy, &(&1.price == order.price)) == nil do
        order = if left > 0, do: %Order{order | amount: left}, else: order
        add_to_output(output, :sell, order)
      else
        output
      end
    end
  end


  @doc ~S"""
  Loop through the buy list looking for partial match and generate the updated list.
  Returns the updated buy list. 
  """
  defp reduce_buy(list, order) do
    Enum.reduce(list, {[], 0}, fn record, {results, left} ->
      if order.price <= record.price do # partial match!
        left = (if left > 0, do: left - record.volume, else: order.amount - record.volume) |> round(3)

        if left < 0 do
          new_record = %Record{price: record.price, volume: abs(left)}
          {results ++ [new_record], left}
        else
          {results, left}
        end
      else
        {results ++ [record], left}
      end
    end)
  end


  @doc ~S"""
  Loop through the sell list looking for partial match and generate the updated list.
  Returns the updated sell list. 
  """
  defp reduce_sell(list, order) do
    Enum.reduce(list, {[], 0}, fn record, {results, left} ->
      if order.price >= record.price do # partial match!
        left = (if left > 0, do: left - record.volume, else: order.amount - record.volume) |> round(3)

        if left < 0 do
          new_record = %Record{price: record.price, volume: abs(left)}
          {results ++ [new_record], left}
        else
          {results, left}
        end
      else
        {results ++ [record], left}
      end
    end)
  end


  @doc ~S"""
  Creates a record struct from an order.
  Returns the struct Record. 
  """
  defp make_record(order) do
    %Record{price: order.price, volume: order.amount}
  end


  @doc ~S"""
  Update a processed buy list into the output map.
  Returns the updated output map. 
  """
  defp add_to_output(output, command, order) when command == :buy,
    do: Map.put(output, command, update_output(order, output.buy, command))


  @doc ~S"""
  Update a processed sell list into the output map.
  Returns the updated output map. 
  """
  defp add_to_output(output, command, order) when command == :sell,
    do: Map.put(output, command, update_output(order, output.sell, command))


  @doc ~S"""
  Insert an order command into the list (buy or sell) when the list is empty.
  Returns the list with the new record. 
  """
  defp update_output(order, list, _command) when list == [], do: [make_record(order)]


  @doc ~S"""
  Insert an order command into the buy list.
  Returns the updated buy list. 
  """
  defp update_output(order, list, command) when is_list(list) and command == :buy do
    [head | tail] = list

    cond do
      order.price > List.first(list).price ->
        [make_record(order) | list]

      order.price < List.last(list).price ->
        list ++ [make_record(order)]

      order.price < head.price ->
        [head] ++ update_output(order, tail, command)

      order.price == head.price ->
        head = %{head | volume: head.volume + order.amount}
        [head | tail]

      order.price > head.price ->
        [make_record(order) | tail]
    end
  end


  @doc ~S"""
  Insert an order command into the sell list.
  Returns the updated sell list. 
  """
  defp update_output(order, list, command) when is_list(list) and command == :sell do
    [head | tail] = list

    cond do
      order.price > List.last(list).price ->
        list ++ [make_record(order)]

      order.price < List.first(list).price ->
        [make_record(order) | list]

      order.price > head.price ->
        [head] ++ update_output(order, tail, command)

      order.price == head.price ->
        head = %{head | volume: head.volume + order.amount}
        [head | tail]

      order.price < head.price ->
        [make_record(order) | tail]
    end
  end
end
