defmodule Order do
  @derive [Poison.Encoder]
  defstruct command: nil,
            price: 0.0,
            amount: 0.0

  @type t :: %Order{
          command: String.t(),
          price: Float.t(),
          amount: Float.t()
        }
end

defmodule Input do
  @derive [Poison.Encoder]
  defstruct orders: [%Order{}]

  @type t :: %Input{
          orders: List.t()
        }
end
