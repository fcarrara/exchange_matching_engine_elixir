defmodule Record do
  defstruct price: nil,
            volume: nil

  @type t :: %Record{
          price: Float.t(),
          volume: Float.t()
        }
end

defmodule Output do
  defstruct buy: [],
            sell: []

  @type t :: %Output{
          buy: List.t(),
          sell: List.t()
        }
end
